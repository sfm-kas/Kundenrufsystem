-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Sep 09, 2018 at 02:50 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `krs`
--
CREATE DATABASE IF NOT EXISTS `krs` DEFAULT CHARACTER SET latin1 COLLATE latin1_german2_ci;
USE `krs`;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `cId` int(4) UNSIGNED NOT NULL,
  `cIn` datetime NOT NULL,
  `cOut` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`cId`, `cIn`, `cOut`) VALUES
(17, '2018-09-09 01:45:22', '2018-09-09 02:12:17'),
(158, '2018-09-09 00:45:05', '2018-09-09 01:43:39'),
(167, '2018-09-09 01:45:20', '2018-09-09 01:47:40'),
(347, '2018-09-09 01:45:19', '2018-09-09 01:56:30'),
(423, '2018-09-09 01:45:17', '2018-09-09 01:48:51'),
(448, '2018-09-09 01:45:17', '2018-09-09 01:55:43'),
(468, '2018-09-09 01:45:24', '2018-09-09 01:55:30'),
(469, '2018-09-09 01:45:23', '2018-09-09 01:47:58'),
(482, '2018-09-09 01:56:16', '2018-09-09 02:12:18'),
(486, '2018-09-09 01:12:44', '2018-09-09 01:55:42'),
(843, '2018-09-09 01:25:19', '2018-09-09 01:44:25'),
(1189, '2018-09-09 01:45:25', '2018-09-09 01:53:31'),
(1573, '2018-09-09 01:45:26', '2018-09-09 01:53:41'),
(4568, '2018-09-09 01:45:16', '2018-09-09 02:12:16'),
(4579, '2018-09-09 01:56:17', '2018-09-09 02:12:19'),
(4771, '2018-09-09 01:56:18', '2018-09-09 02:12:20'),
(4789, '2018-09-09 01:45:22', '2018-09-09 01:53:40'),
(19894, '2018-09-09 01:45:21', '2018-09-09 01:47:09'),
(31458, '2018-09-09 01:45:28', '2018-09-09 01:56:31'),
(48912, '2018-09-09 01:12:24', '2018-09-09 01:43:45'),
(466666, '2018-09-09 01:45:35', '2018-09-09 01:55:29');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `cSetting` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `cValue` text COLLATE latin1_german2_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`cSetting`, `cValue`) VALUES
('displayCore.updateOrderListInterval', '5000'),
('orderOverdueTimeout', '5000');

-- --------------------------------------------------------

--
-- Table structure for table `waitAnimations`
--

DROP TABLE IF EXISTS `waitAnimations`;
CREATE TABLE `waitAnimations` (
  `cFileName` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `cLastUsed` datetime NOT NULL DEFAULT '1970-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- Dumping data for table `waitAnimations`
--

INSERT INTO `waitAnimations` (`cFileName`, `cLastUsed`) VALUES
('0397_5416.gif', '1970-01-01 00:00:00'),
('0nbshSk.gif', '1970-01-01 00:00:00'),
('0PojhPx.gif', '1970-01-01 00:00:00'),
('1cg34.gif', '1970-01-01 00:00:00'),
('1KbTh2b.gif', '1970-01-01 00:00:00'),
('2200_842b.gif', '1970-01-01 00:00:00'),
('2273_cf69.gif', '1970-01-01 00:00:00'),
('5167_1264.gif', '1970-01-01 00:00:00'),
('6.gif', '1970-01-01 00:00:00'),
('6HkUiC8.gif', '1970-01-01 00:00:00'),
('6z2NXUR.gif', '1970-01-01 00:00:00'),
('8KpXqwx.gif', '1970-01-01 00:00:00'),
('9EMSwfG.gif', '1970-01-01 00:00:00'),
('9MZQW.gif', '1970-01-01 00:00:00'),
('adfq.gif', '1970-01-01 00:00:00'),
('BEUOvOx.gif', '1970-01-01 00:00:00'),
('bl7ozGP.gif', '1970-01-01 00:00:00'),
('CMHPS3A.gif', '1970-01-01 00:00:00'),
('EjSceVb.gif', '1970-01-01 00:00:00'),
('elQmA2o.gif', '1970-01-01 00:00:00'),
('eYMKi53.gif', '1970-01-01 00:00:00'),
('FAKQzFF.gif', '1970-01-01 00:00:00'),
('fdrjo.gif', '1970-01-01 00:00:00'),
('giphy.gif', '1970-01-01 00:00:00'),
('giphy0.gif', '1970-01-01 00:00:00'),
('giphy1.gif', '1970-01-01 00:00:00'),
('giphy10.gif', '1970-01-01 00:00:00'),
('giphy11.gif', '1970-01-01 00:00:00'),
('giphy12.gif', '1970-01-01 00:00:00'),
('giphy13.gif', '1970-01-01 00:00:00'),
('giphy14.gif', '1970-01-01 00:00:00'),
('giphy16.gif', '1970-01-01 00:00:00'),
('giphy17.gif', '1970-01-01 00:00:00'),
('giphy18.gif', '1970-01-01 00:00:00'),
('giphy18_.gif', '1970-01-01 00:00:00'),
('giphy19.gif', '1970-01-01 00:00:00'),
('giphy21.gif', '1970-01-01 00:00:00'),
('giphy22.gif', '1970-01-01 00:00:00'),
('giphy23.gif', '1970-01-01 00:00:00'),
('giphy24.gif', '1970-01-01 00:00:00'),
('giphy25.gif', '1970-01-01 00:00:00'),
('giphy26.gif', '1970-01-01 00:00:00'),
('giphy27.gif', '1970-01-01 00:00:00'),
('giphy28.gif', '1970-01-01 00:00:00'),
('giphy28_.gif', '1970-01-01 00:00:00'),
('giphy29.gif', '1970-01-01 00:00:00'),
('giphy3.gif', '1970-01-01 00:00:00'),
('giphy30.gif', '1970-01-01 00:00:00'),
('giphy32.gif', '1970-01-01 00:00:00'),
('giphy33.gif', '1970-01-01 00:00:00'),
('giphy34.gif', '1970-01-01 00:00:00'),
('giphy35.gif', '1970-01-01 00:00:00'),
('giphy36.gif', '1970-01-01 00:00:00'),
('giphy37.gif', '1970-01-01 00:00:00'),
('giphy38_.gif', '1970-01-01 00:00:00'),
('giphy4.gif', '1970-01-01 00:00:00'),
('giphy40.gif', '1970-01-01 00:00:00'),
('giphy41.gif', '1970-01-01 00:00:00'),
('giphy42.gif', '1970-01-01 00:00:00'),
('giphy43.gif', '1970-01-01 00:00:00'),
('giphy44.gif', '1970-01-01 00:00:00'),
('giphy45.gif', '1970-01-01 00:00:00'),
('giphy46.gif', '1970-01-01 00:00:00'),
('giphy47.gif', '1970-01-01 00:00:00'),
('giphy48.gif', '1970-01-01 00:00:00'),
('giphy48_.gif', '1970-01-01 00:00:00'),
('giphy49.gif', '1970-01-01 00:00:00'),
('giphy5.gif', '1970-01-01 00:00:00'),
('giphy50.gif', '1970-01-01 00:00:00'),
('giphy51.gif', '1970-01-01 00:00:00'),
('giphy52.gif', '1970-01-01 00:00:00'),
('giphy53.gif', '1970-01-01 00:00:00'),
('giphy54.gif', '1970-01-01 00:00:00'),
('giphy55.gif', '1970-01-01 00:00:00'),
('giphy56.gif', '1970-01-01 00:00:00'),
('giphy57.gif', '1970-01-01 00:00:00'),
('giphy58.gif', '1970-01-01 00:00:00'),
('giphy58_.gif', '1970-01-01 00:00:00'),
('giphy59.gif', '1970-01-01 00:00:00'),
('giphy6.gif', '1970-01-01 00:00:00'),
('giphy60.gif', '1970-01-01 00:00:00'),
('giphy61.gif', '1970-01-01 00:00:00'),
('giphy62.gif', '1970-01-01 00:00:00'),
('giphy63.gif', '1970-01-01 00:00:00'),
('giphy64.gif', '1970-01-01 00:00:00'),
('giphy65.gif', '1970-01-01 00:00:00'),
('giphy66.gif', '1970-01-01 00:00:00'),
('giphy67.gif', '1970-01-01 00:00:00'),
('giphy68.gif', '1970-01-01 00:00:00'),
('giphy68_.gif', '1970-01-01 00:00:00'),
('giphy69.gif', '1970-01-01 00:00:00'),
('giphy7.gif', '1970-01-01 00:00:00'),
('giphy70.gif', '1970-01-01 00:00:00'),
('giphy8.gif', '1970-01-01 00:00:00'),
('giphy8_.gif', '1970-01-01 00:00:00'),
('giphy9.gif', '1970-01-01 00:00:00'),
('HEHWqVj.gif', '1970-01-01 00:00:00'),
('hnDFWde.gif', '1970-01-01 00:00:00'),
('ijHRdMZ.gif', '1970-01-01 00:00:00'),
('logo_stempel.gif', '1970-01-01 00:00:00'),
('mgc.gif', '1970-01-01 00:00:00'),
('n3.gif', '1970-01-01 00:00:00'),
('su65.gif', '1970-01-01 00:00:00'),
('tenor.gif', '1970-01-01 00:00:00'),
('TFMBQ.gif', '1970-01-01 00:00:00'),
('tgORo41.gif', '1970-01-01 00:00:00'),
('UE5VPbM.gif', '1970-01-01 00:00:00'),
('us1cM80.gif', '1970-01-01 00:00:00'),
('yaf245h6h.gif', '2018-09-09 02:50:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`cId`,`cIn`) USING BTREE;

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD UNIQUE KEY `cSetting` (`cSetting`);

--
-- Indexes for table `waitAnimations`
--
ALTER TABLE `waitAnimations`
  ADD PRIMARY KEY (`cFileName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
