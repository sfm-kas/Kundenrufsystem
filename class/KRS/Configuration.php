<?php

namespace MonkeyVoodoo\KRS;

use MonkeyVoodoo\KRS\Exceptions\ConfException\KrsConfExDBConfigDamaged;
use MonkeyVoodoo\KRS\Exceptions\ConfException\KrsConfigFileNotFound;

/**
 * Holds all information and configurations needed during runtime to operate correctly
 */
class Configuration
{
    /** @var  Configuration $instance Holds a static instance of the Configuration class */
    private static $instance;
    /** @var  array $database Database configuration */
    private $database;


    /**
     * Configuration constructor.
     * @throws KrsConfigFileNotFound
     */
    public function __construct()
    {
        $this->loadConfigFile( $this->database, 'db.conf.php' );
    }

    /**
     * Return a singleton instance of the Configuration class
     *
     * @return Configuration
     * @throws KrsConfigFileNotFound
     */
    public static function getInstance(): Configuration
    {
        if( self::$instance === null ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns database username
     *
     * @return string
     * @throws KrsConfExDBConfigDamaged
     */
    public function getDBUser(): string
    {
        return $this->getDbServerSetting( 'user' );
    }

    /**
     * Returns database password
     *
     * @return string
     * @throws KrsConfExDBConfigDamaged
     */
    public function getDBPw(): string
    {
        return $this->getDbServerSetting( 'password' );
    }

    /**
     * Returns database host
     *
     * @return string
     * @throws KrsConfExDBConfigDamaged
     */
    public function getDBHost(): string
    {
        return $this->getDbServerSetting( 'host' );
    }

    /**
     * Returns database Name
     *
     * @return string
     * @throws KrsConfExDBConfigDamaged
     */
    public function getDBName(): string
    {
        return $this->getDbServerSetting( 'name' );
    }

    /**
     * Returns database config name (for debug use only)
     *
     * Due to the fact that the config/db.conf.php file can hold more then one database configuration,
     * we return the name of the used configuration
     *
     * @return string
     * @throws KrsConfExDBConfigDamaged
     */
    public function getDBConfigName(): string
    {
        return $this->getDbSetting( 'server' );
    }

    /**
     * Returns the db table Prefix for tables
     *
     * @return string
     * @throws KrsConfExDBConfigDamaged
     */
    public function getDBTablePrefix(): string
    {
        return $this->getDbSetting( 'tablePrefix' );
    }

    /**
     * Returns the complete database config (db.conf.php)
     *
     * @return array
     */
    public function getDatabaseConfig(): array
    {
        return $this->database[ 'servers' ][ $this->database[ 'server' ] ];
    }

    /**
     * Loads a default config file and adds the user config if available
     *
     * @param String[] $targetProperty Target property of the Configuration class
     * @param string $sourceFile Name of the config file (without path)
     *
     * @throws KrsConfigFileNotFound
     */
    private function loadConfigFile( &$targetProperty, $sourceFile )
    {
        if( !isset( $sourceFile ) || empty( $sourceFile ) ) {
            throw new KrsConfigFileNotFound( $sourceFile );
        }

        $defaultConfigFile = __ROOT__ . 'inc/' . $sourceFile;

        if( !file_exists( $defaultConfigFile ) ||
            !is_readable( $defaultConfigFile )
        ) {
            throw new KrsConfigFileNotFound( $defaultConfigFile );
        }

        $targetProperty = require $defaultConfigFile;

        $userConfigFile = __ROOT__ . 'config/' . $sourceFile;
        if( file_exists( $userConfigFile ) && is_readable( $userConfigFile ) ) {
            $userConfig = require $userConfigFile;
            if( is_array( $userConfig ) ) {
                $tools = new Tools();
                $targetProperty = $tools->arrayMergeRecursiveDistinct( $targetProperty, $userConfig );
            }
        }
    }

    /**
     * Returns the wanted database server config setting
     *
     * @param string $settingName Array key of the database server setting
     *
     * @return mixed
     * @throws KrsConfExDBConfigDamaged
     */
    private function getDbServerSetting( $settingName )
    {
        $dbConfigName = $this->getDBConfigName();

        if( !array_key_exists( 'servers', $this->database ) ) {
            throw new KrsConfExDBConfigDamaged( 'servers' );
        }

        if( !array_key_exists( $dbConfigName, $this->database[ 'servers' ] ) ) {
            throw new KrsConfExDBConfigDamaged( $dbConfigName );
        }

        $dbConf = &$this->database[ 'servers' ][ $dbConfigName ];

        if( !array_key_exists( $settingName, $dbConf ) ) {
            throw new KrsConfExDBConfigDamaged( $settingName );
        }

        return $dbConf[ $settingName ];
    }

    /**
     * Returns the wanted database server config setting
     *
     * @param string $settingName Array key of the database setting
     *
     * @return mixed
     * @throws KrsConfExDBConfigDamaged
     */
    private function getDbSetting( $settingName )
    {
        if( !array_key_exists( $settingName, $this->database ) ) {
            throw new KrsConfExDBConfigDamaged( $settingName );
        }

        return $this->database[ $settingName ];
    }
}
