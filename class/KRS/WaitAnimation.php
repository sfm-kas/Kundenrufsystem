<?php
/**
 * Created by PhpStorm.
 * User: thora
 * Date: 08.09.18
 * Time: 22:15
 */

namespace MonkeyVoodoo\KRS;

use DateTime;

class WaitAnimation
{

    public static function refreshFileList()
    {
        $db = DatabaseHandler::getInstance();

        $files = array();
        foreach (glob(__ROOT__ . "gifs/*.gif") as $file) {
            $files[][":fileName"] = basename($file);
        }

        $sqlTruncate = "TRUNCATE `krs`.`waitAnimations`";
        $db->executeStatement($sqlTruncate);

        $sqlInsert = "INSERT INTO `waitAnimations` (`cFileName`) VALUES (:fileName);";
        $db->executeMultiRowStatement($sqlInsert, $files);
    }

    public static function get_deprecated()
    {
        $db = DatabaseHandler::getInstance();

        $fetchLastUsed = "SELECT * FROM waitAnimations order by cLastUsed DESC limit 1";
        $lastUsed = $db->fetchAllAssoc($fetchLastUsed);

        if(!key_exists(0, $lastUsed)) return "logo.gif";

        $lastUsedDateTime = new DateTime($lastUsed[0]["cLastUsed"]);
        $diff = $lastUsedDateTime->diff(new DateTime());
        if(!$diff->s < 60) return $lastUsed[0];

        $fetchNext = "SELECT * FROM waitAnimations order by cLastUsed ASC limit 1";
        $next = $db->fetchAllAssoc($fetchNext);

        if(!key_exists(0, $next)) return "logo.gif";

        $updateNext = "UPDATE `waitAnimations` SET `cLastUsed` = NOW() WHERE `waitAnimations`.`cFileName` = :fileName;";
        $parameters = [ ":fileName" => $next[0]["cFileName"] ];
        $db->executeStatement($updateNext, $parameters);

        return $next[0];
    }

    public static function get()
    {
        return "logo.gif";
        /*/
        $db = DatabaseHandler::getInstance();

        $fetchAnimationFiles = "SELECT *, (cLastUsed - CURRENT_TIMESTAMP) *-1 as cTimeDiff
                                FROM   waitAnimations AS a, 
                                (SELECT cFileName AS nextFile FROM waitAnimations order by cLastUsed ASC limit 1) AS next,
                                (SELECT cFileName AS lastFile FROM waitAnimations order by cLastUsed DESC limit 1) AS last
                                WHERE  next.nextFile = a.cFileName
                                OR last.lastFile = a.cFileName
                                ORDER BY cTimeDiff ASC;";
        $animationFiles = $db->fetchAllAssoc($fetchAnimationFiles);

        if(!key_exists(0, $animationFiles) || !key_exists(1, $animationFiles)) return "logo.gif";

        if($animationFiles[0]['cTimeDiff'] < 60) return $animationFiles[0];

        $updateNext = "UPDATE `waitAnimations` SET `cLastUsed` = CURRENT_TIMESTAMP WHERE `waitAnimations`.`cFileName` = :fileName;";
        $parameters = [ ":fileName" => $animationFiles[1]["cFileName"] ];
        $db->executeStatement($updateNext, $parameters);

        return $animationFiles[1];
        //*/
    }
}