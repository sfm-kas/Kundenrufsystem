<?php

namespace MonkeyVoodoo\KRS\Exceptions\ToolsException;

use Exception;
use RuntimeException;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsToolsExStringToBoolFailed extends RuntimeException
{
    /**
     * KrsToolsExStringToBoolFailed constructor.
     *
     * @param Exception $previous Previous caught exception
     */
    public function __construct(Exception $previous = null)
    {
        $msg = "String zu Boolean Konvertierung fehlgeschlagen. Der String entsprach keinem gültigen Boolean.";

        parent::__construct($msg, ErrorCodes::TOOLS_STR_TO_BOOL_FAILED, $previous);
    }
}
