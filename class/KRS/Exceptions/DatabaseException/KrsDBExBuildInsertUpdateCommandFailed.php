<?php

namespace MonkeyVoodoo\KRS\Exceptions\DatabaseException;

use Exception;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsDBExBuildInsertUpdateCommandFailed extends Exception
{
    /**
     * KrsDBExBuildInsertUpdateCommandFailed constructor.
     *
     * @param Exception $previous
     */
    public function __construct(Exception $previous = null)
    {
        $msg = "Zusammenbau eines Datenbankbefehls fehlgeschlagen.";
        parent::__construct($msg, ErrorCodes::DB_BUILD_INSERT_UPDATE_COMMAND_FAILED, $previous);
    }
}