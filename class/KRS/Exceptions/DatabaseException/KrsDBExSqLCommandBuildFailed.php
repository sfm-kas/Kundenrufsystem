<?php

namespace MonkeyVoodoo\KRS\Exceptions\DatabaseException;

use Exception;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsDBExSqLCommandBuildFailed extends Exception
{
    /**
     * KrsDBExSqLCommandBuildFailed constructor.
     *
     * @param Exception $previous
     */
    public function __construct(Exception $previous = null)
    {
        $msg = "Ausführung eines Datenbankbefehls fehlgeschlagen.";
        parent::__construct($msg, ErrorCodes::DB_COMMAND_BUILD_FAILED, $previous);
    }
}