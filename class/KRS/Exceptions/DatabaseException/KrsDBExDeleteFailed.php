<?php

namespace MonkeyVoodoo\KRS\Exceptions\DatabaseException;

use Exception;
use RuntimeException;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsDBExDeleteFailed extends RuntimeException
{
    /**
     * KrsDBExDeleteFailed constructor.
     *
     * @param Exception $previous Previous caught exception
     */
    public function __construct(Exception $previous = null)
    {
        $msg = "Löschen der Daten fehlgeschlagen.";
        parent::__construct($msg, ErrorCodes::DB_DELETE_FAILED, $previous);
    }
}
