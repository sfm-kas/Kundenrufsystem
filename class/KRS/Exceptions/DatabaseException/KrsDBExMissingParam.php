<?php

namespace MonkeyVoodoo\KRS\Exceptions\DatabaseException;

use Exception;
use RuntimeException;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsDBExMissingParam extends RuntimeException
{
    /**
     * KrsDBExMissingParam constructor.
     *
     * @param array     $parameters Array with ['missing'] and ['needless'] parameter list
     * @param Exception $previous   Previous caught exception
     */
    public function __construct($parameters, Exception $previous = null)
    {
        $missingCount = 0;
        $missing = "";

        $needlessCount = 0;
        $needless = "";

        if( array_key_exists('missing', $parameters) )
        {
            $missingCount = count($parameters[ 'missing' ]);
            $missing = " (" . implode(", ", $parameters[ 'missing' ]) . ") ";
        }

        if( array_key_exists('needless', $parameters) )
        {
            $needlessCount = count($parameters[ 'needless' ]);
            $needless = " (" . implode(", ", $parameters[ 'needless' ]) . ")";
        }

        $msg = "Es gibt " . $missingCount . " fehlende Parameter" . $missing . " und "
            . $needlessCount . " überflüssige Parameter" . $needless . ".";

        parent::__construct($msg, ErrorCodes::DB_MISSING_PARAMETER, $previous);
    }
}
