<?php

namespace MonkeyVoodoo\KRS\Exceptions\DatabaseException;

use Exception;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsDBExGetParametersFromRowFailed extends Exception
{
    /**
     * KrsDBExGetParametersFromRowFailed constructor.
     *
     * @param Exception $previous
     */
    public function __construct(Exception $previous = null)
    {
        $msg = "Zusammenbau eines Datenbankbefehls fehlgeschlagen.";
        parent::__construct($msg, ErrorCodes::DB_GET_PARAM_FROM_ROW_FAILED, $previous);
    }
}