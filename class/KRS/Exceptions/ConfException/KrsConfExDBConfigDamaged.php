<?php

namespace MonkeyVoodoo\KRS\Exceptions\ConfException;

use Exception;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsConfExDBConfigDamaged extends Exception
{
    /**
     * KrsConfExDBConfigDamaged constructor.
     *
     * @param string    $missingSetting Name of the setting that is missing in the db config array
     * @param Exception $previous       Previous caught exception
     */
    public function __construct($missingSetting, Exception $previous = null)
    {
        $message = "Konfigurationsdatei 'db.conf.php' ist nicht vollständig. Es fehlt eine Einstellung: " . $missingSetting;

        parent::__construct($message, ErrorCodes::CONF_DB_CONF_DAMAGED, $previous);
    }
}
