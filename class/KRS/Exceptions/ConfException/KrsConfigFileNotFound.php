<?php

namespace MonkeyVoodoo\KRS\Exceptions\ConfException;

use Exception;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsConfigFileNotFound extends Exception
{
    /**
     * KrsConfigFileNotFound constructor.
     *
     * @param string    $missingConfigFile Name of the file that went Mission
     * @param Exception $previous          Previous caught exception
     */
    public function __construct($missingConfigFile, Exception $previous = null)
    {
        $message = "Konfigurationsdatei nicht gefunden: " . $missingConfigFile;

        parent::__construct($message, ErrorCodes::CONF_FILE_NOT_FOUND, $previous);
    }
}
