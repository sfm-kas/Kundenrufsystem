<?php

namespace MonkeyVoodoo\KRS\Exceptions;

class ErrorCodes
{
    // ParametersException 1xx
    const VALIDATION_MISSING_PARAMETER = '100';
    const VALIDATION_WRONG_PARAMETER_TYPE = '101';
    const VALIDATION_NOT_AN_OBJECT = '102';
    // DBException 2xx
    const DB_QUERY_EXECUTION_FAILED = '200';
    const DB_DELETE_FAILED = '201';
    const DB_MISSING_PARAMETER = '202';
    const DB_COMMAND_BUILD_FAILED = '203';
    const DB_BUILD_INSERT_UPDATE_COMMAND_FAILED = '204';
    const DB_GET_PARAM_FROM_ROW_FAILED = '205';
    // UserException 3xx
    const USER_PERMISSION_NOT_FOUND = '300';
    const USER_PERMISSION_UPDATE_FAILED = '301';
    const USER_WRONG_PASSWORD = '302';
    const USER_PERMISSION_DELETE_FAILED_NO_USER_ID = '303';
    const USER_ALREADY_EXISTS = '304';
    const USER_NOT_FOUND = '305';
    const USER_IMAGE_UPLOAD_FAILED = '306';
    // LdapException 4xx
    const LDAP_CONFIG_EMPTY = '400';
    const LDAP_CONFIG_MISSING_SETTING = '401';
    const LDAP_SET_VERSION_FAILED = '402';
    // ProcessPointsException 5xx
    const PP_NO_DATA = '500';
    const PP_WRONG_GRID_TYPE = '501';
    // ChartException 6xx
    const CHART_NO_VISUAL_DATA = '600';
    // UserChartException 7xx
    const USER_CHART_PERSIST_FAILED = '700';
    const USER_CHART_DAMAGED_DATA = '701';
    // TableException 8xx
    const TABLE_NO_DATA = '800';
    const TABLE_WRONG_GRID_TYPE = '801';
    // EventsException 9xx
    const EVENTS_NO_DATA = '900';
    // ManualUserAdjustmentsException 10xx
    const MUA_WRONG_PP_TYPE = '1000';
    // ConfigException 11xx
    const CONF_CHECKSUM_CALC_FAILED = '1100';
    const CONF_FILE_NOT_FOUND = '1101';
    const CONF_WEBDAT_CONF_DAMAGED = '1102';
    const CONF_CUST_CONF_DAMAGED = '1103';
    const CONF_DB_CONF_DAMAGED = '1104';
    // UserTableException 12xx
    const USER_TABLE_DAMAGED_DATA = '1200';
    const USER_TABLE_PERSIST_FAILED = '1201';
    // PipeBreakMonitoringException 13xx
    const PBM_L_PBMD_ID_WAS_NULL = '1300';
    const PBM_D_MISSING_PROPERTY = '1301';
    const PBM_L_MISSING_PROPERTY = '1302';
    const PBM_A_MISSING_PROPERTY = '1303';
    const PBM_SAVE_DATA_CORRUPTED = '1304';
    const PBM_D_PERSISTING_FAILED = '1305';
    const PBM_L_PERSISTING_FAILED = '1406';
    const PBM_A_PERSISTING_FAILED = '1407';
    const PBM_DAMAGED_LIMITS_DATA = '1408';
    const PBM_NO_COMPARABLES_FOUND = '1409';
    const PBMD_DELETE_FAILED = '1410';
    // LicenceException 14xx
    const LICENCE_READ_FAILED = '1400';
    const LICENCE_CORRUPTED = '1401';
    const LICENCE_FILE_NOT_FOUND = '1402';
    const LICENCE_KEY_FILE_NOT_FOUND = '1403';
    const LICENCE_EXPIRED = '1404';
    // ToolsException 15xx
    const TOOLS_STR_TO_BOOL_FAILED = '1500';
    // WebDATException 16xx
    const WEBDAT_MODULE_NOT_LICENCED = '1600';
    // ModuleException 17xx
    const MODULE_MISSING_INIT = '1700';
    // Sessionmanager 18XX
    const SESSION_VARIABLE_NOT_FOUND = '1800';
    // Password 19XX
    const PASSWORD_SALT_GENERATING_FAILED = '1900';
    // Tasks 20xx
    const TASKS_USER_ALREADY_ASSIGNED = '2000';
}
