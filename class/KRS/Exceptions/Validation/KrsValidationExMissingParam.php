<?php

namespace MonkeyVoodoo\KRS\Exceptions\Validation;

use Exception;
use InvalidArgumentException;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsValidationExMissingParam extends InvalidArgumentException
{
    /**
     * KrsValidationExMissingParam constructor.
     *
     * @param string    $falseParameter Missing of false parameters
     * @param Exception $previous       Previous caught exception
     */
    public function __construct($falseParameter, Exception $previous = null)
    {
        $msg = "Nicht alle notwendigen Parameter wurden übergeben." .
            "<br>Fehlender Parameter: " . $falseParameter;

        parent::__construct($msg, ErrorCodes::VALIDATION_MISSING_PARAMETER, $previous);
    }
}