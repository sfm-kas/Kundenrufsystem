<?php

namespace MonkeyVoodoo\KRS\Exceptions\Validation;

use Exception;
use RuntimeException;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsValidationExNotAnObject extends RuntimeException
{
    /**
     * KrsValidationExNotAnObject constructor.
     *
     * @param string    $typeWas
     * @param Exception $previous
     */
    public function __construct($typeWas, Exception $previous = null)
    {
        $msg = "Objekt Validierung fehlgeschlagen. Anstelle eines Objektes wurde '" . $typeWas . "' vorgefunden";

        parent::__construct($msg, ErrorCodes::VALIDATION_NOT_AN_OBJECT, $previous);
    }
}
