<?php

namespace MonkeyVoodoo\KRS\Exceptions\Validation;

use Exception;
use InvalidArgumentException;
use MonkeyVoodoo\KRS\Exceptions\ErrorCodes;
use Throwable;

class KrsValidationExWrongParamType extends InvalidArgumentException
{
    /**
     * KrsValidationExWrongParamType constructor.
     *
     * @param string    $parameter    Parameter name
     * @param string    $typeWas      The type the parameter was
     * @param string    $typeShouldBe The type the parameter should be
     * @param Exception $previous     Previous caught exception
     */
    public function __construct($parameter, $typeWas, $typeShouldBe, Exception $previous = null)
    {
        $msg = "Der Parameter '" . $parameter . "' ist vom Typ '" . $typeWas
            . "', er sollte aber vom Typ '" . $typeShouldBe . "' sein.";

        parent::__construct($msg, ErrorCodes::VALIDATION_WRONG_PARAMETER_TYPE, $previous);
    }
}
