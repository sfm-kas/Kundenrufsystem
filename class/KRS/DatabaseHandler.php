<?php

namespace MonkeyVoodoo\KRS;

use Exception;
use PDO;
use MonkeyVoodoo\KRS\Exceptions\DatabaseException\KrsDBExBuildInsertUpdateCommandFailed;
use MonkeyVoodoo\KRS\Exceptions\DatabaseException\KrsDBExDeleteFailed;
use MonkeyVoodoo\KRS\Exceptions\DatabaseException\KrsDBExGetParametersFromRowFailed;
use MonkeyVoodoo\KRS\Exceptions\DatabaseException\KrsDBExMissingParam;
use MonkeyVoodoo\KRS\Exceptions\DatabaseException\KrsDBExSqLCommandBuildFailed;

/**
 * Handels all requests to the Database and does some small work like returning IDs
 */
class DatabaseHandler
{
    /** @var  DatabaseHandler $instance Holds a singleton instance of the database handler */
    private static $instance;
    /** @var PDO $database Database PDO resource */
    private $database;
    /** @var array $dbConf Database configuration */
    private $dbConf;
    /** @var array $lastQueries Holds the last executed database queries for debug readons */
    private $lastQueries = [];

    /**
     * DatabaseHandler constructor.
     *
     * Establishes a database connection for further work
     */
    public function __construct()
    {
        $this->dbConf = Configuration::getInstance()->getDatabaseConfig();
        $this->connectToDB();
    }

    /**
     * Return a singleton instance of the DatabaseHandler class
     *
     * @return DatabaseHandler
     */
    public static function getInstance(): DatabaseHandler
    {
        if( self::$instance === null )
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Commit pending data
     *
     * The intended use of __sleep() is to commit pending data or perform similar cleanup tasks. Also, the function
     * is useful if you have very large objects which do not need to be saved completely.
     *
     * @return array
     */
    public function __sleep()
    {
        $this->disconnect();

        return [ 'database', 'dbConf', 'lastQueries' ];
    }

    /**
     * Reestablish any database connections
     *
     * The intended use of __wakeup() is to reestablish any database connections that may have been lost during
     * serialization and perform other reinitialization tasks.
     */
    public function __wakeup()
    {
        $this->connectToDB();
    }

    /**
     * Returns the last inserted ID from the DB (mysql: LAST_INSERT_ID())
     */
    public function fetchLastInsertedID()
    {
        $sqlSelect = "SELECT LAST_INSERT_ID() AS LAST_ID;";
        $sqlResult = $this->fetchAllAssoc($sqlSelect);

        return $sqlResult[ 0 ][ "LAST_ID" ];
    }

    /**
     * Execute a sql statement and return the result as assoc array
     *
     * @param string $sqlQuery      SQL select statement
     * @param array  $parameterList (optional) values for the use parameters in the statement
     *
     * @return array
     */
    public function fetchAllAssoc($sqlQuery, $parameterList = [])
    {
        $parameters = $parameterList;
        $sql = $sqlQuery;

        foreach( $parameters as $parameterName => $parameterValue )
        {
            if( is_array($parameterValue) )
            {
                $newArgList = [];
                foreach( $parameterValue as $deepParameterValue )
                {
                    $newParameterName = $this->getRandomParameterName();
                    $newArgList[] = $newParameterName;
                    $parameters[ $newParameterName ] = $deepParameterValue;
                }
                $newArgListString = implode(' , ', $newArgList);
                $sql = str_replace($parameterName, $newArgListString, $sql);
                unset($parameters[ $parameterName ]);
            }
        }

        $result = $this->executeStatement($sql, $parameters);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        return $rows;
    }

    /**
     * Execute an prepared database statement
     *
     * @param string $statement  SQL statement
     * @param array  $parameters (optional) If $statement is a prepared statement you have to provide the parameters in
     *                           an array
     *
     * @return \PDOStatement  Returns the result as an PDOStatement object
     * @throws Exception
     */
    public function executeStatement($statement, $parameters = [])
    {
        $this->checkDBConnection();
        if( !$this->database->inTransaction() )
        {
            $this->database->beginTransaction();
        }

        //check if all parameters are set
        $newQuery = str_replace(array_keys($parameters), array_values($parameters), $statement);
        $paramCheckResult = $this->checkParameters($statement, $parameters);
        if( is_array($paramCheckResult) )
        {
            throw new KrsDBExMissingParam($paramCheckResult);
        }

        $preparedStatement = $this->database->prepare($statement);
        $this->lastQueries[] = $newQuery;
        $preparedStatement->execute($parameters);
        $this->commitDB();

        return $preparedStatement;
    }

    /**
     * Executes a multiple row select.
     *
     * This means that the $statement will be executed for every parameter array in $parameters.
     *
     * @param string $statement SQL statement
     * @param array  $parameters
     */
    public function executeMultiRowStatement($statement, $parameters = [])
    {
        foreach( $parameters as $parameter )
        {
            $this->executeStatement($statement, $parameter);
        }
    }

    /**
     * Returns the value of the selected column in the first matching row.
     *
     * conditions input array example:
     * $conditions = [  "column"  => "some_column_name",
     *                  "value"   => 12345,
     *                  "operator" => ">=" ]
     *
     * @param string $column     column to look for
     * @param string $table      table in the opal03 database
     * @param array  $conditions array full of conditions
     *
     * @return null|mixed
     */
    public function fetchSingleValue($column, $table, $conditions)
    {
        $conditionsAndParams = $this->prepareWhereConditionAndParameters($conditions);

        $sqlSelect = "SELECT " . $column . " FROM opal03." . $table . $conditionsAndParams[ "sql" ] . " LIMIT 0,1";

        $sqlResult = $this->executeStatement($sqlSelect, $conditionsAndParams[ "parameters" ]);
        $result = $sqlResult->fetch();

        if( count($result) > 0 )
        {
            return $result[ 0 ];
        }

        return null;
    }

    /**
     * Returns a single row from the database
     *
     * @param string $columns    Columns for the select, separated by coma
     * @param string $table      Table name for the select
     * @param array  $conditions Where condition for the select, [ "column" => "id", "value" => 1, "operator" => "=" ];
     *
     * @return array Returns the result or null if no row was found at all
     */
    public function fetchSingleRow($columns, $table, $conditions)
    {
        $conditionsAndParams = $this->prepareWhereConditionAndParameters($conditions);

        $sqlSelect = "SELECT " . $columns . " FROM " . $this->dbConf[ 'name' ] . "." . $table . $conditionsAndParams[ "sql" ] . " LIMIT 0,1";

        $sqlResult = $this->executeStatement($sqlSelect, $conditionsAndParams[ "parameters" ]);
        $result = $sqlResult->fetch();

        if( count($result) > 0 )
        {
            return $result;
        }

        return null;
    }

    /**
     * Updates a single value in the database
     *
     * conditions input array example:
     * $conditions = [  "column"   => "some_column_name",
     *                  "value"    => 12345,
     *                  "operator" => ">=" ]
     *
     * @param string $column     column name where to write the new value
     * @param mixed  $value      new value
     * @param string $table      table name of a table in the opal03 database
     * @param array  $conditions where condition
     */
    public function updateSingleValue($column, $value, $table, $conditions)
    {
        $conditionsAndParams = $this->prepareWhereConditionAndParameters($conditions);

        $preparedSetStatement = $this->prepareSetStatementPart($column);

        $parameters = array_merge($conditionsAndParams[ "parameters" ],
            [ $preparedSetStatement[ "paramName" ] => $value ]);

        $sqlUpdate = "UPDATE " . $table .
            " SET " . $preparedSetStatement[ "sql" ] .
            " " . $conditionsAndParams[ "sql" ];

        $this->executeStatement($sqlUpdate, $parameters);
    }

    /**
     * Insert new Rows into a database table. If the keys already exist, the rows will be updated.
     *
     * @param string   $table Target table name
     * @param string[] $newRows
     *
     * @throws KrsDBExSqLCommandBuildFailed
     */
    public function insertOrUpdate($table, $newRows)
    {
        if( count($newRows) < 1 )
        {
            throw new KrsDBExSqLCommandBuildFailed();
        }

        $parameterNamesString = null;
        $sqlColumnsString = null;
        $updateCommandString = null;
        $sqlParameters = null;

        foreach( $newRows as $row )
        {
            $sqlColumnsArray = array_keys($row);
            $sqlColumnsString = implode(', ', $sqlColumnsArray);

            $sqlParameters[] = $this->buildParameterArrayFromDbRow($row);
            $parameterNamesString = implode(', ', array_keys($sqlParameters[ 0 ]));

            $updateCommands = $this->buildInsertUpdateCommand($sqlColumnsArray);
            $updateCommandString = implode(', ', $updateCommands);
        }

        $sqlStatement = 'INSERT INTO ' . $table . ' (' . $sqlColumnsString . ') ' .
            'VALUES(' . $parameterNamesString . ') ON DUPLICATE KEY UPDATE ' . $updateCommandString;

        $this->executeMultiRowStatement($sqlStatement, $sqlParameters);
    }

    /**
     * Delete a single row from the database
     *
     * @param string     $table  Table name
     * @param string     $column Column name, mostly the key column for where condition
     * @param string|int $rowID  Value in the column field for where condition
     *
     * @return int  Number of effected rows
     */
    public function deleteSingleRow($table, $column, $rowID)
    {
        try
        {
            $sqlStatement = "DELETE FROM " . $this->dbConf[ 'name' ] . "." . $table . " WHERE " . $column . " = :" . $column . " LIMIT 1";
            $sqlParameter = [ ':' . $column => $rowID ];

            $sqlResult = $this->executeStatement($sqlStatement, $sqlParameter);
        }
        catch(\PDOException $e)
        {
            throw new KrsDBExDeleteFailed($e);
        }

        return $sqlResult->rowCount();
    }

    /**
     * Splits the selected date range into multiple ranges if the selection spans over the year boundary.
     * So "18.09.2015 - 24.01.2016" gets "18.09.2015 - 31.12.2015, 01.01.2016 - 24.01.2016".
     *
     * @param \DateTime $start Start date
     * @param \DateTime $end   End date
     *
     * @return \DateTime[] Split date ranges
     */
    public function calculateSelectRangesForEDOrEMOrEY($start, $end)
    {
        $result = [];
        while( $start->format('Y') < $end->format('Y') )
        {
            $tempDate = clone($start);
            $tempDate->setDate($start->format('Y'), 12, 31);
            $result[] = [ 'start' => clone($start), 'end' => clone($tempDate) ];

            $start->add(new \DateInterval('P1Y'));
            $start->setDate($start->format('Y'), 1, 1);
        }

        $result[] = [ 'start' => clone($start), 'end' => clone($end) ];

        return $result;
    }

    /**
     * Splits the selected date range into multiple ranges if the selection spans over the month boundary.
     * So "18.11.2015 - 24.01.2016" gets "18.11.2015 - 30.11.2015, 01.11.2015 - 31.11.2015, 01.01.2016 - 24.01.2016".
     *
     * @param \DateTime $start Start date
     * @param \DateTime $end   End date
     *
     * @return array<\DateTime> Split date ranges
     */
    public function calculateSelectRangesForEQOrEH($start, $end)
    {
        $result = [];
        while( $start->format('Ym') < $end->format('Ym') )
        {
            $tempDate = clone($start);
            $tempDate->setDate($start->format('Y'), $start->format('m'), $start->format('t'));
            $result[] = [ 'start' => clone($start), 'end' => clone($tempDate) ];

            $start->setDate($start->format('Y'), $start->format('m'), 1);
            $start->add(new \DateInterval('P1M'));
        }

        $result[] = [ 'start' => clone($start), 'end' => clone($end) ];

        return $result;
    }

    /**
     * Direct query the Database. ATTENTION: You have to know what you do!
     *
     * @param string $sqlQuery Some SQL Query
     *
     * @return \PDOStatement
     */
    public function query($sqlQuery)
    {
        $this->checkDBConnection();
        if( !$this->database->inTransaction() )
        {
            $this->database->beginTransaction();
        }

        $sqlResult = $this->database->query($sqlQuery);

        return $sqlResult;
    }

    /**
     * Connect to the database mentioned in the config/opal.conf.php file
     */
    private function connectToDB()
    {
        $dbOptions = [ PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                       PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                       PDO::ATTR_EMULATE_PREPARES   => true ];

        $dbHost = "host=" . $this->dbConf[ "host" ] . ";";
        $dbName = "dbname=" . $this->dbConf[ "name" ];

        $dbConnectionString = "mysql:" . $dbHost . $dbName;

        $this->database = new PDO($dbConnectionString, $this->dbConf[ "user" ], $this->dbConf[ "password" ], $dbOptions);
    }

    /**
     * Return a random string of this pattern ':[a-z]'
     *
     * @return string
     */
    private function getRandomParameterName()
    {
        return ":" . substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 8);
    }

    /**
     * Check if the database connection is still alive
     *
     * @throws Exception
     */
    private function checkDBConnection()
    {
        if( !isset($this->database) )
        {
            $this->connectToDB();

            if( !isset($this->database) )
            {
                throw new Exception('No DB connection found.');
            }
        }
    }

    /**
     * Checks an parameter array against the sql query. it will tell you if there are any missing or needless parameters
     *
     * @param string $query      Sql query
     * @param array  $parameters Parameters array
     *
     * @return bool|array Returns TRUE if no missing or needless parameters where found or a list with the missing
     *                    or needless parameters
     */
    private function checkParameters($query, $parameters)
    {
        $parameterTMP = $parameters;
        $parameterCount = count($parameterTMP);
        $regexMatches = null;
        $regexMatchCounter = preg_match_all("/:[^%]\\D\\w*/", $query, $regexMatches);

        // if there are parameter in the $parameters array oder parameters in the sql query
        if( $parameterCount > 0 || $regexMatchCounter > 0 )
        {
            $result = [];

            // take every parameter found in the sql query
            foreach( $regexMatches[ 0 ] as $parameterName )
            {
                // check if the required parameter is in the parameters array
                if( !array_key_exists($parameterName, $parameters) )
                {
                    // if it is not in the parameters array, add it to the list of missing parameters
                    // and continue with the next parameter from the query
                    $result[ 'missing' ][] = $parameterName;
                    continue;
                }

                // if the required parameter is in the parameter array, delete it from this array
                // so we get a list of parameters that are needless
                unset($parameterTMP[ $parameterName ]);
            }

            // check if there are (needless) parameters left
            if( count($parameterTMP) > 0 )
            {
                // if so, add them to the list of needles parameters
                $result[ 'needless' ] = array_keys($parameterTMP);
            }

            // if at this point $result is an array,
            // some parameters are missing or needless, so we return the result list(s)
            if( count($result) > 0 )
            {
                return $result;
            }
        }

        // if we reach this point, no missing or needless parameters where found,
        // you are good to go
        return true;
    }

    /**
     * Commit the database
     */
    private function commitDB()
    {
        $this->database->commit();
    }

    /**
     * Prepares a sql where statement part and a parameter list
     *
     * conditions input array example:
     * $conditions = [  "column"  => "some_column_name",
     *                  "value"   => 12345,
     *                  "operator" => ">=" ]
     *
     * @param array $conditions array with conditions [[ column, value, operator],...]
     *
     * @return array
     */
    private function prepareWhereConditionAndParameters($conditions)
    {
        if( !array_key_exists(0, $conditions) )
        {
            $conditionsTmp = $conditions;
            $conditions = [];
            $conditions[] = $conditionsTmp;
        }
        $sqlConditionAndParam = [];
        $sqlConditionAndParam[ "sql" ] = "";
        $sqlConditionAndParam[ "parameters" ] = [];
        if( count($conditions) <= 0 )
        {
            return $sqlConditionAndParam;
        }

        foreach( $conditions as $condition )
        {
            $sqlConditionAndParam[ "sqlTMP" ][] = " " . $condition[ "column" ] .
                " " . $condition[ "operator" ] .
                " :" . $condition[ "column" ];

            $sqlConditionAndParam[ "parameters" ][ ":" . $condition[ "column" ] ] = $condition[ "value" ];
        }

        $sqlConditionAndParam[ "sql" ] = " WHERE";
        $sqlConditionAndParam[ "sql" ] .= implode(" AND", $sqlConditionAndParam[ "sqlTMP" ]);

        unset($sqlConditionAndParam[ "sqlTMP" ]);

        return $sqlConditionAndParam;
    }

    /**
     * Builds the update part of an SQL insert statement
     *
     * @param string[] $columnNames Column names of the table
     *
     * @return array
     * @throws KrsDBExBuildInsertUpdateCommandFailed
     */
    private function buildInsertUpdateCommand($columnNames)
    {
        $updateCommands = null;

        foreach( $columnNames as $columnName )
        {
            $updateCommands[] = $columnName . '=VALUES(' . $columnName . ')';
        }

        if( $updateCommands === null )
        {
            throw new KrsDBExBuildInsertUpdateCommandFailed();
        }

        return $updateCommands;
    }

    /**
     * Builds an sql parameter array from an table row
     *
     * @param string[] $row Table row
     *
     * @return array
     * @throws KrsDBExGetParametersFromRowFailed
     */
    private function buildParameterArrayFromDbRow($row)
    {
        $rowParameters = null;
        foreach( $row as $column => $value )
        {
            $rowParameters[ ':' . $column ] = $value;
        }

        if( $rowParameters === null )
        {
            throw new KrsDBExGetParametersFromRowFailed();
        }

        return $rowParameters;
    }

    /**
     * Prepares the SET statement part of an sql UPDATE
     *
     * @param string $column Column name
     *
     * @return string[]
     */
    private function prepareSetStatementPart($column)
    {
        $paramName = $this->getRandomParameterName();

        $prepValue = [];
        $prepValue[ "sql" ] = $column . " = " . $paramName;
        $prepValue[ "paramName" ] = $paramName;

        return $prepValue;
    }

    /**
     * Close all open database connections
     */
    private function disconnect()
    {
        $this->database = null;
    }
}
