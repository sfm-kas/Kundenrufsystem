<?php
namespace MonkeyVoodoo\KRS;

class Order
{
    private static $instance;

    /** @var DatabaseHandler $db */
    private $db;

    public function __construct()
    {
        $this->db = DatabaseHandler::getInstance();
    }

    /**
     * Return a singleton instance of the Order class
     *
     * @return Order
     */
    public static function getInstance(): Order
    {
        if( self::$instance === null )
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function in(int $orderId)
    {
        $sqlInsert = "INSERT INTO krs.orders (cId, cIn, cOut) VALUES (:orderId, NOW(), NULL);";//"  ON DUPLICATE KEY UPDATE cId = cId, cIn = NOW(), cOut = null;";
        $sqlParameters = [":orderId" => $orderId ];

        $this->db->executeStatement($sqlInsert, $sqlParameters);
    }

    public function out(int $orderId)
    {
        $sqlUpdate = "UPDATE krs.orders SET cOut = NOW() WHERE cId = :orderId and cOut IS NULL;";
        $sqlParameters = [":orderId" => $orderId ];

        $this->db->executeStatement($sqlUpdate, $sqlParameters);

    }

    public function remove(int $orderId)
    {
        $sqlRemove = "DELETE FROM orders WHERE cId = :orderId AND cOut IS NULL";
        $sqlParameters = [":orderId" => $orderId ];

        $this->db->executeStatement($sqlRemove, $sqlParameters);
    }

    public function list()
    {
        $sqlSelect = "SELECT * FROM krs.orders WHERE cOut IS NULL ORDER BY cIn ASC LIMIT 0,8;";

        return $this->db->fetchAllAssoc($sqlSelect);
    }
}