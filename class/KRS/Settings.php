<?php
namespace MonkeyVoodoo\KRS;

class Settings
{
    public static function fetch() : array
    {
        $sqlQuery = "SELECT * FROM krs.settings;";

        $db = DatabaseHandler::getInstance();
        $result = $db->fetchAllAssoc($sqlQuery);

        $tools = new Tools();
        return $tools->convertNumericArrayToSimpleAssoc($result, "cSetting", "cValue");
    }

    public static function set(string $setting, string $value)
    {
        $newRows = [[
            $setting =>  $value
        ]];

        $db = \MonkeyVoodoo\KRS\DatabaseHandler::getInstance();
        $db->insertOrUpdate("krs.settings", $newRows);
    }
}