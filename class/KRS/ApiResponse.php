<?php
namespace MonkeyVoodoo\KRS;

class ApiResponse
{
   /** @var ApiResponse $instance Holds a singleton instance of the api class */
   private static $instance;
   /** @var array ApiResponse response array */
   private $response;
   
   /**
    * ApiResponse constructor.
    */
   public function __construct()
   {
       @set_error_handler("errorHandler", E_ALL - E_NOTICE);
       @set_exception_handler('exceptionHandler'); 
       $this->response  = [ "status" => "success" ];
   }

   /**
    * Return a singleton instance of the ApiResponse class
    *
    * @return ApiResponse
    */
   public static function getInstance(): ApiResponse
   {
       if( self::$instance === null )
       {
           self::$instance = new self();
       }

       return self::$instance;
   }

   public static function errorHandler($nr, $error, $file, $line, array $context)
   {        
        $contextString = "\n\nContext:\n " . print_r($context, true); // @codingStandardsIgnoreLine

        $errorException = new ErrorException($error . $contextString, 0, $nr, $file, $line);
        ApiResponse::addError($errorException);
        ApiResponse::set(ApiReturnStatus::error);
        echo ApiResponse::getJson();
        die();
    } 

    public static function exceptionHandler($exception) 
   {        
        ApiResponse::addError($exception);
        ApiResponse::set(ApiReturnStatus::error);
        echo ApiResponse::getJson();
        die();
    } 

   public function add($response)
   {
       $this->response["response"] = $response;
   }

   public function addError($error)
   {
       $this->response["errors"][] = $error;
   }

   public function getJson()
   {
       return json_encode($this->response);
   }
}
