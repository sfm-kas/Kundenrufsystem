<?php

$errorHandler = function($errno, $errstr, $errfile, $errline, array $errcontext) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
};

set_error_handler($errorHandler, E_ALL - E_NOTICE);

$exceptionHandler = function($exception)
{
    /** @var Exception $exception **/
    echo "<pre><b>[Exception " . $exception->getCode() . "]\n<h1>" . $exception->getMessage();
    echo "</h1></b>\n" . $exception->getFile() . "(" . $exception->getLine() . ")" . "\n";
    echo $exception->getTraceAsString() . "\n" . $exception->getPrevious() . "</pre>";
};

// TODO beim umstieg auf php7 muss das hier angepasst werden
// http://php.net/manual/en/migration70.incompatible.php   <-- set_exception_handler()
set_exception_handler($exceptionHandler);