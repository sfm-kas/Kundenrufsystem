<?php

function rootPath()
{
    $rootPath = str_replace('\\', '/', __DIR__);
    $rootPath = preg_replace('|(?<=.)/+|', '/', $rootPath);
    if( ':' === substr($rootPath, 1, 1) )
    {
        $rootPath = ucfirst($rootPath);
    }

    return $rootPath;
}

define("__ROOT__", rootPath() . '/../');
