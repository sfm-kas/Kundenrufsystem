<?php
//server settings
return [
    'server'      => 'local', // Server bezeichnung aus der liste 'servers' die für die Datenbankverbindungen verwendet wird
    'servers'     => [
        'local'      => [                  // config name -> muss in opal.conf.php eingetragen werden !

            'host'     => '127.0.0.1',     // DB Host
            'name'     => 'krs',        // DB Name
            'user'     => 'krs',          // DB Benutzer
            'password' => 'krs'         // DB Passwort
        ]
    ],
    'tablePrefix' => ""
];
