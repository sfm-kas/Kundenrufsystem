<?php
require_once "./../api.php";

if(!isset($_POST) || !key_exists("orderId", $_POST) )
{
    throw new \MonkeyVoodoo\KRS\Exceptions\Validation\KrsValidationExMissingParam("orderId");
}

$order = \MonkeyVoodoo\KRS\Order::getInstance();
$order->remove($_POST["orderId"]);

$responseArray = ["status" => "removed", "orderId" => $_POST["orderId"]];

$response->add($responseArray);

echo $response->getJson();
