<?php
require_once "./../api.php";

if(!isset($_POST) || !key_exists("settingName", $_POST)|| !key_exists("settingValue", $_POST))
{
    throw new \MonkeyVoodoo\KRS\Exceptions\Validation\KrsValidationExMissingParam("settingName or settingValue");
}

Settings::set($_POST["settingName"],$_POST["settingValue"]);

setReturnStatus(ApiReturnStatus::success);
