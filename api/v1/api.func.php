<?php
/**
 * Converts an Exception into a JSON response message
 *
 * @param Exception $exception
 */
function convertExceptionIntoNotification( $exception )
{
    $message = "<b>[Exception " . $exception->getCode() . "]<br>" . $exception->getMessage()
        . "<br><br>" . $exception->getFile() . "(" . $exception->getLine() . ")</b><br><br><b>Stack trace:</b><br>"
        . $exception->getTraceAsString() . "<br><br><b>Previous:</b><br>" . $exception->getPrevious();

    addResponse( "errors", $message );
}

/**
 * Returns a JSON response message
 *
 * @param string $key Response key
 * @param mixed $content The actual response content for the response key
 */
function addResponse( $key, $content )
{
    global $returnObj;

    if(key_exists($key, $returnObj))
    {
        if(!is_array($returnObj[$key]))
        {
            $returnObj[$key][] = $returnObj[$key];
        }

        $returnObj[$key][] = $content;
        return;
    }

    $returnObj[ $key ] = $content;
}

function setReturnStatus(string $returnStatus)
{
    global $returnObj;
    $returnObj["status"] = $returnStatus;
}

class ApiReturnStatus
{
    public const success = "success";
    public const error = "error";
    public const fatal = "fatal";
    public const warning = "warning";
    public const unknown = "unknown";
}
