<?php

function errorHandler($nr, $error, $file, $line, array $context)
{

    $contextString = "\n\nContext:\n " . print_r($context, true); // @codingStandardsIgnoreLine

    $errorException = new ErrorException($error . $contextString, 0, $nr, $file, $line);
    ApiResponse::addError($errorException);
    ApiResponse::set(ApiReturnStatus::error);
    echo ApiResponse::getJson();
    die();
}

function exceptionHandler($exception) 
{
    require_once __DIR__ . '/../../class/KRS/ApiResponse.php';

    $response = \MonkeyVoodooesponse::getInstance();
    $response->addError($exception);
    $response->set(ApiReturnStatus::error);
    echo $response->getJson();
    die();
}

set_error_handler("errorHandler", E_ALL - E_NOTICE);
set_exception_handler("exceptionHandler");