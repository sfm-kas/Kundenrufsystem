<?php
header('Content-Type: application/json');

require_once  __DIR__ . "/../../inc/base.conf.php";

require_once __ROOT__ . 'class/autoloader.php';
require_once __ROOT__ . 'api/v1/api.func.php';

$response = \MonkeyVoodoo\KRS\ApiResponse::getInstance();

$databaseHandler = \MonkeyVoodoo\KRS\DatabaseHandler::getInstance();

$tools = new \MonkeyVoodoo\KRS\Tools();

// prevent browser cache, because of IE11
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
