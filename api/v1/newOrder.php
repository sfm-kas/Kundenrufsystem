<?php
require_once "api.php";

if(!isset($_POST) || !key_exists("newOrderId", $_POST))
{
    throw new \MonkeyVoodoo\KRS\Exceptions\Validation\KrsValidationExMissingParam("newOrderId");
}

$order = new \MonkeyVoodoo\KRS\Order();
$order->new($_POST["newOrderId"]);

setReturnStatus(ApiReturnStatus::success);
