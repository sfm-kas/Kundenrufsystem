krs.namespace('krs.CounterCore');
krs.CounterCore = (function()
{
    "use strict";
    let _displayHistory = true;
    let _displayNumpad = true;

    let _tblColSidebar = $("#mainRight");
    let _tblHistory = $("#tblRowHistory");
    let _tblNumpad = $("#tblRowNumPad");

    let _showHistory = function()
    {
        _tblHistory.show()
    };

    let _hideHistory = function()
    {
        _tblHistory.hide();
    };

    let _showNumpad = function()
    {
        _tblNumpad.show();
    };

    let _hideNumpad = function()
    {
        _tblNumpad.hide();
    };

    let _showSidebar = function()
    {
        _tblColSidebar.show();
    };

    let _hideSidebar = function()
    {
        _tblColSidebar.hide();
    };

    let _initDone = function()
    {
        krs.log( '[krs.CounterCore] init done', 'debug', false );
    };

    let _init = function()
    {
        _displayHistory ? _showHistory() : _hideHistory();
        _displayNumpad ? _showNumpad() : _hideNumpad();

        (_displayHistory || _displayNumpad ) ? _showSidebar() : _hideSidebar();

        _initDone()
    };

    $( document ).ready( _init );

})();