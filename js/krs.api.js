krs.api = (function()
{
    "use strict"
    let _defaultVersion = "v1";

    let _dummyCallback = function apiDummyCallback() {
        krs.log('no api callback was set, so this empty dummy was called', 'debug', false);
    };

    /**
     * Call the krs api via GET
     *
     * @param {string}   apiTarget
     * @param {string}   apiParameters
     * @param {function} callBack
     * @param {string}   apiVersion
     */
    let _get = function(apiTarget, callBack = _dummyCallback, apiParameters = "", apiVersion = _defaultVersion)
    {
        $.getJSON("./../api/" + apiVersion + "/" + apiTarget + ".php?" + apiParameters, function(apiResponse)
        {            
            if(!apiResponse.hasOwnProperty('status') || apiResponse.status !== "success")
            {
                krs.log("API response status: " + apiResponse.status, "error", true);
                krs.log(apiResponse, "error");

                return;
            }

            callBack(apiResponse.response);

        }).fail(function(apiResponse)
        {
            krs.log("API call failed!", "error", true);
            krs.log(apiResponse, "error");
        });
    };

    let _getSync = function(apiTarget, callBack = _dummyCallback, apiParameters = "", apiVersion = _defaultVersion)
    {
        $.ajaxSetup({
            async: false
        });

        _get(apiTarget, callBack, apiParameters, apiVersion);

        $.ajaxSetup({
            async: true
        });
    };

    /**
     * Call the krs api via POST
     *
     * @param {string}   apiTarget
     * @param {function} callBack
     * @param {object}   payload
     */
    let _post = function(apiTarget, payload, callBack = _dummyCallback, apiVersion = _defaultVersion)
    {
        
        let contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
        let processData = true;

        if(payload instanceof FormData)
        {
            // this is needed for image upload !
            contentType = false;
            processData = false;
        }

        // do not try to improve this by using jQery.post()!
        // jQery.post() can not handle file uploads from forms!
        // todo ignore comment above and try to improve this, because we don't need file uploads atm
        $.ajax({
            url: "./../api/" + apiVersion + "/" + apiTarget + ".php",
            type: 'POST',
            data: payload,
            async: true,
            success: function(apiResponse)
            {
                if(!apiResponse.hasOwnProperty('status') || apiResponse.status !== "success")
                {
                    krs.log("API response status: " + apiResponse.status, "error", true);
                    krs.log(apiResponse, "error");
                    return;
                }
    
                callBack(apiResponse.response);
            },
            cache: false,
            contentType: contentType,
            processData: processData,
            error: function(apiResponse)
            {

                krs.log("API call failed!", "error", true);
                krs.log(apiResponse, "error");
            },
            complete: function()
            {
            }
        });
    };

    let _initDone = function()
    {
        krs.log('[krs.api] init done', 'debug', false);
    };

    let _init = function()
    {
        _initDone();
    }();

    $(document).ready(_init);

    return {
        get: _get,
        getSync: _getSync,
        post: _post
    }

})();