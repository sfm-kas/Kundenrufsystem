let krs = {};
krs = (function(refToSelf)
{
    "use strict";
    let _debugMode = false;
    let _keypress = new window.keypress.Listener();
    let _pageRefresh = "init";

    let _namespace = function(namespaceString)
    {
        let parts = namespaceString.split('.'),
            parent = refToSelf,
            i;

        // strip redundant leading global
        if(parts[0] === 'krs')
        {
            parts = parts.slice(1);
        }

        for(i = 0; i < parts.length; i += 1)
        {
            // create a property if it doesn't exist
            if(typeof parent[parts[i]] === 'undefined')
            {
                parent[parts[i]] = {};
            }
            parent = parent[parts[i]];
        }

        return parent;
    };

    let _isDebugModeOn = function()
    {
        return _debugMode;
    };


    let _reportApiNotifications = function(notifications)
    {
        // todo proper notification handling
        _log("API Notifications:", "debug");
        _log(notifications, "debug");
    };

    let _log = function(msg, type, printStack)
    {
        if(typeof printStack === 'undefined')
        {
            printStack = false;
        }

        let msgWasObject = false;
        let theObj = msg;
        if(typeof msg === 'object')
        {
            msg = '[OBJECT]';
            msgWasObject = true;
        }

        switch(type.toLowerCase())
        {
            case 'warn':
                msg = '[WARNING] \t' + msg;
                break;
            case 'error':
                msg = '[ERROR] \t' + msg;
                break;
            case 'info':
                msg = '[INFO] \t' + msg;
                break;
            case 'debug':
                if(!_isDebugModeOn())
                {
                    return;
                }
                msg = '[DEBUG] \t' + msg;
                break;
            case 'log':
            default:
                msg = '[LOG] \t' + msg;
                break;
        }

        let calledFrom = _wasCalledFrom();
        msg = '[' + calledFrom + '] \t' + msg;

        console.log(msg);
        if(msgWasObject)
        {
            console.log(theObj);
        }

        if(printStack === true)
        {
            console.log(new Error().stack);
        }
    };

    /**
     * Returns the line number in which the function was called.
     *
     * @param {int} offset How many calls do you want to go back in the call stack?
     * @returns {Number}
     */
    let _wasCalledAtLine = function(offset)
    {
        let stack = new Error().stack.split('\n');
        let line = stack[(offset || 1) + 1].split(':');
        return parseInt(line[line.length - 2], 10);
    };

    /**
     * Returns the file name in which the function was called.
     *
     * @param {int} offset How many calls do you want to go back in the call stack?
     * @returns {string}
     */
    let _wasCalledFromFile = function(offset)
    {
        let stack = new Error().stack.split('\n');
        let line = stack[(offset || 1) + 1].split(':');

        let url = line[line.length - 3];

        let filename = url.substring(url.lastIndexOf('/') + 1);
        let parameterPosition = filename.indexOf('?');
        if(parameterPosition !== -1)
        {
            return filename.substring(0, parameterPosition);
        }
        return filename;
    };

    /**
     * Returns the filename and the line number from which the second last function was called.
     *
     * @returns {string}
     */
    let _wasCalledFrom = function()
    {
        let offset = 2;
        let line = _wasCalledAtLine(offset);
        let file = _wasCalledFromFile(offset);

        return file + ':' + line;
    };

    let _initDone = function()
    {
        _log('[krs.core] init done', 'debug', false);
    };

    let _checkForPageRefresh = function()
    {
        if(krs.hasOwnProperty("CounterCore"))
        {
            //Do not refresh counter and kitchen devices
            return;
        }

        $.getJSON("./../pageRefresh.json", function(apiResponse)
        {
            if(!apiResponse.hasOwnProperty('doRefresh'))
            {
                krs.log("API response status: " + apiResponse.status, "error", true);
                krs.log(apiResponse, "error");

                return;
            }

            if(_pageRefresh === "init") {
                _pageRefresh = apiResponse.doRefresh;
                return;
            }

            if(_pageRefresh === apiResponse.doRefresh) return;

            location.reload(true);

        }).fail(function(apiResponse)
        {
            krs.log("API call failed!", "error", true);
            krs.log(apiResponse, "error");
        });
    };

    let _init = function()
    {
        _debugMode = true;
        $.ajaxSetup({ cache: false });
        setInterval( _checkForPageRefresh, 10000);
        _initDone();
    }();

    $(document).ready(_init);

    return {
        log: _log,
        isDebugModeOn: _isDebugModeOn,
        namespace: _namespace,
        keypress: _keypress,
    }

}(krs));