krs.namespace( 'krs.history' );
krs.history = (function()
{
    "use strict"
    let _history = $( '#history' );

    let _scrollDown = function()
    {
        //$(elem).scrollTop = elem.scrollHeight;
        //todo only set intervall when _history is displayed
        //if(!_history.visible) return;
        window.setInterval( function()
        {
            var elem = document.getElementById( 'historyContainer' );
            elem.scrollTop = elem.scrollHeight;
        }, 500 );
    };

    let _cleanUp = function()
    {
        if( _history.find( 'li' ).length > 500 )
        {
            _history.find( 'li' ).first().remove();
        }
    };


    let _add = function( orderId, command, msg, style )
    {
        let timeStr = dayjs().format( 'HH:mm:ss' );
        let _tmpOrderId = orderId;
        if(orderId.length < 5)
        {
            _tmpOrderId = ('&nbsp;&nbsp;&nbsp;&nbsp;' + orderId).slice(orderId.length * 6);
        }
        _history.append( '<li><span class="historyTimeStamp ' + style + '">[' + timeStr + ']</span> <b>' + command + '</b>&nbsp;' + _tmpOrderId + '&nbsp;' + msg + '</li>' );
        _cleanUp();
    };

    let _orderAdded = function( orderId)
    {
        let msg = 'eingetragen';
        let style = 'historyOrderEntered';
        _add(orderId, '+', msg, style);
    };

    let _orderRemoved = function( orderId )
    {
        let msg = 'ausgetragen';
        let style = "historyOrderRemoved";
        _add(orderId, '-', msg, style);

    };

    let _orderFetched = function( orderId )
    {

        let msg = 'ausgetragen';
        let style = "historyOrderRemoved";
        _add(orderId, '*', msg, style);
    };

    let _initDone = function()
    {
        krs.log( '[krs.history] init done', 'debug', false );
    };

    let _init = function()
    {
        _scrollDown();
        _initDone();
    }();

    return {
        orderAdded: _orderAdded,
        orderRemoved: _orderRemoved,
        orderFetched: _orderFetched
    }
})();
