krs.namespace( 'krs.CounterButtons' );
krs.CounterButtons = (function()
{
    "use strict";
    let _buttonConfirm = true,
        _buttonTimeOut = 0, //in ms
        _overdueTimeOut = 180000, //in ms
        _refreshInterval = 1000, //in ms
        _buttonCount = 12,

        _refreshActive = false,
        _updateActive = false,
        _refreshCountdown = 0,
        _buttons = [
            $("#btn0"),
            $("#btn1"),
            $("#btn2"),
            $("#btn3"),
            $("#btn4"),
            $("#btn5"),
            $("#btn6"),
            $("#btn7"),
            $("#btn8"),
            $("#btn9"),
            $("#btn10"),
            $("#btn11"),
        ];


    let _remove = function()
    {
        for( let btnObj of $( '.btnRemoveMarker' ) )
        {
            let btn = $( btnObj );
            let now = new Date().getTime();
            if( ((now - btn.data( "markedOn" )) > _buttonTimeOut) || _buttonTimeOut == null )
            {
                _clearByObj( btn );
                krs.api.post("order/out",{orderId: btn.val()});
                btn.val( null );
            }
        }
    };

    let _clearByObj = function( button )
    {
        let btn = $( button );
        btn.addClass("killing");
        btn.prop( "disabled", true );
        btn.html( "<b class=\"cId\"></b><br><i class=\"warningTimer\"></i>" );
        btn.removeClass( "btnOverdue" );
        btn.removeClass( "btnRemoveMarker" );
        btn.removeClass( "btnWarning" );
        btn.data( "clickCount", 0 );
        btn.data( "markedOn", 0 );
        btn.data("markedWarning", 0);
        btn.data( "added", 0 );
        btn.removeClass("killing");
        //btn.hide();
    };

    let _clearByNr = function( nr )
    {
        let btn = $( "#btn" + nr );
        _clearByObj( btn );
    };

    let _MarkForRemoval = function( button )
    {
        let btn = $( button );
        btn.addClass( "btnRemoveMarker" );
        btn.data( "markedOn", new Date().getTime() );
        btn.removeClass( "btnOverdue" );

        //btn.fadeOut( _buttonTimeOut );

        if( _buttonTimeOut == null || !Number.isInteger( _buttonTimeOut ) )
        {
          _remove();
          //  btn.fadeOut( _buttonTimeOut );
        }
    };

    let _removeRemovalMarker = function( button )
    {
        let btn = $( button );
        btn.removeClass( "btnRemoveMarker" );
        btn.removeClass( "btnWarning" );
        btn.data( "markedOn", 0 );
        btn.data( "clickCount", 0 );
    };

    let _onClick = function()
    {
        let btn = $( this );

        if( btn.hasClass( "btnRemoveMarker" ) )
        {

            _removeRemovalMarker( btn );
            btn.stop();
            btn.fadeToggle( 1 );
            return;
        }

        if( _buttonConfirm && (typeof btn.data( "clickCount" ) === "undefined" || btn.data( "clickCount" ) === 0) )
        {
            $(".btnWarning").data("clickCount", 0).removeClass("btnWarning") ;

            btn.data( "clickCount", 1 );
            btn.data( "markedWarning", new Date().getTime() );
            //btn.removeClass( "btnOverdue" );
            btn.addClass( "btnWarning" );
            return;
        }

        if( _buttonConfirm && (typeof btn.data( "clickCount" ) !== "undefined" || btn.data( "clickCount" ) === 1) )
        {
            if(new Date().getTime() - btn.data("markedWarning") < 500) return;
        }

        btn.addClass( "btnWarning" );

        _MarkForRemoval( btn );
    };

    let _startRemovalInterval = function()
    {
        if( !Number.isInteger( _buttonTimeOut ) )
        {
            return;
        }

        setInterval( _remove, _buttonTimeOut );
    };

    let _msToString = function( ms )
    {
        let seconds = (ms / 1000).toFixed( 0 );
        let minutes = Math.floor( seconds / 60 );
        let hours = "";
        if( minutes > 59 )
        {
            hours = Math.floor( minutes / 60 );
            hours = (hours >= 10) ? hours : "0" + hours;
            minutes = minutes - (hours * 60);
            minutes = (minutes >= 10) ? minutes : "0" + minutes;
        }

        seconds = Math.floor( seconds % 60 );
        seconds = (seconds >= 10) ? seconds : "0" + seconds;
        if( hours !== "" )
        {
            return hours + ":" + minutes + ":" + seconds;
        }
        return minutes + ":" + seconds;
    };

    let _refresh = function()
    {
        for(let button in _buttons)
        {
            if(!_buttons.hasOwnProperty(button)) continue;

            if( _buttons[button].prop( "disabled" ) || _buttons[button].hasClass("killing" ))
            {
                continue;
            }

            let warningTimer = dayjs().valueOf() - dayjs(_buttons[button].data( "added" )).valueOf();
            let timer = _msToString( warningTimer );
            _buttons[button].find( '.warningTimer' ).html("(" + timer + ")" );
            _buttons[button].find( '.cId' ).html( _buttons[button].val() );

            if( warningTimer > _overdueTimeOut && !_buttons[button].hasClass( 'btnWarning' ) )
            {
                _buttons[button].addClass( 'btnOverdue' );
            }
        }
    };

    let _update = function( orders )
    {

        $(".btnOrderNr").removeClass("btnWarning").data("clickCount", 0).data("markedWarning", 0).prop( "disabled", true );

        let i = 0;
        for(let order in orders)
        {
            if(!orders.hasOwnProperty(order)){
                i++;
                continue;
            }

            _buttons[i].hide();
            _buttons[i].val( orders[ order ]["cId"]);
            _buttons[i].data( "added", orders[ order ]["cIn"] );
            _buttons[i].prop( 'disabled', false );
            _buttons[i].show(0);
            i++
        }

        for( ; i < _buttonCount; i++ )
        {
            _clearByNr( i );
        }

        _refresh();
    };

    let _fetchList = function()
    {
        krs.api.get("order/list", _update);

        //apiGet('latest');

    };

    let _startRefreshInterval = function()
    {
        setInterval( _refresh, _refreshInterval );
    };

    let _updateInterval = function()
    {
        _refreshCountdown--;
        document.getElementById( "countdown" ).innerHTML = _refreshCountdown.toString();

        if( _refreshCountdown <= 0 )
        {
            _fetchList();
            _refreshCountdown = _refreshInterval / 100;
        }

    };

    let _startUpdateInterval = function()
    {
        setInterval( _updateInterval, 1000 );
    };

    let _onBtnKeypress = function()
    {
        return false;
    };

    let _initDone = function()
    {
        krs.log( '[krs.CounterButtons] init done', 'debug', false );
    };

    let _init = function()
    {
        let orderOutTest = $( "#orderOutTest" );
        orderOutTest.on( "click", '.btnOrderNr', _onClick );
        orderOutTest.on( "keypress", '.btnOrderNr', _onBtnKeypress );

        _startRemovalInterval();

        _startUpdateInterval();

        _startRefreshInterval();

        _initDone();
    };

    $( document ).ready( _init );

    return {
        init: _init,
    };

})();