krs.namespace('krs.numpad');
krs.numpad = (function()
{
    "use strict";
    let _display = $('#numPadInputDisplay');

    let _clearDisplay = function()
    {
        _display.val('');
    };

    let _apiCallBack = function(apiResponse)
    {
        switch(apiResponse.status)
        {
            case "added":
                krs.history.orderAdded(apiResponse.orderId);
                break;
            case "removed":
                krs.history.orderRemoved(apiResponse.orderId);
                break;
            case "fetched":
                krs.history.orderFetched(apiResponse.orderId);
                break;
        }
    };

    let _enter = function()
    {
        let displayValue = _display.val();
        if(displayValue.length <= 0)
        {
            return;
        }

        let command = '+';
        if(displayValue.substr(0,1) === '-')
        {
            command = '-';
        }

        let apiPayload = {
            orderId: displayValue.replace(/^\+|^\-/g, '')
        };

        switch(command)
        {
            case '+':
                krs.api.post("order/in", apiPayload, krs.numpad.apiCallBack);
                break;
            case '-':
                krs.api.post("order/remove", apiPayload, krs.numpad.apiCallBack);
                break;
        }

        _clearDisplay();
    };

    let _clicked = function()
    {
        let btnStr = $(this).val();
        //krs.log('Numpad clicked: ' + btnStr, 'debug', false);

        if(btnStr === 'clear')
        {
            _clearDisplay();
            return;
        }

        if(btnStr === 'backspace')
        {
            _display.val(_display.val().substr(0, _display.val().length-1));
            return;
        }

        if(btnStr === 'enter')
        {
            _enter();
            return;
        }

        if (btnStr === '+' && _display.val() === '-')
        {
            _display.val("");
        }

        if (btnStr === '-' && _display.val() === '+')
        {
            _display.val("");
        }

        if ( _display.val().length > 0 && ( btnStr === '+' || btnStr === '-') )
        {
            return;
        }

        _display.val(_display.val() + btnStr);
    };

    let _onBtnKeypress = function () {
        return false;
    };

    let _initDone = function()
    {
        krs.log('[krs.numpad] init done', 'debug', false);
    };

    let _registerOnClickEvents = function()
    {
        document.getElementById("numPadBtn1").addEventListener("click", _clicked);
        document.getElementById("numPadBtn1").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn2").addEventListener("click", _clicked);
        document.getElementById("numPadBtn2").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn3").addEventListener("click", _clicked);
        document.getElementById("numPadBtn3").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn4").addEventListener("click", _clicked);
        document.getElementById("numPadBtn4").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn5").addEventListener("click", _clicked);
        document.getElementById("numPadBtn5").addEventListener("keypress", _onBtnKeypress);
        
        document.getElementById("numPadBtn6").addEventListener("click", _clicked);
        document.getElementById("numPadBtn6").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn7").addEventListener("click", _clicked);
        document.getElementById("numPadBtn7").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn8").addEventListener("click", _clicked);
        document.getElementById("numPadBtn8").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn9").addEventListener("click", _clicked);
        document.getElementById("numPadBtn9").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtn0").addEventListener("click", _clicked);
        document.getElementById("numPadBtn0").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtnClear").addEventListener("click", _clicked);
        document.getElementById("numPadBtnClear").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtnBackspace").addEventListener("click", _clicked);
        document.getElementById("numPadBtnBackspace").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtnMinus").addEventListener("click", _clicked);
        document.getElementById("numPadBtnMinus").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtnPlus").addEventListener("click", _clicked);
        document.getElementById("numPadBtnPlus").addEventListener("keypress", _onBtnKeypress);

        document.getElementById("numPadBtnEnter").addEventListener("click", _clicked);
        document.getElementById("numPadBtnEnter").addEventListener("keypress", _onBtnKeypress);
    };

    let _clickButton = function (btnId) {
        $("#"+btnId).click()
    };

    let _registerKeyboardEvents = function () {

        let buttonMapping = [
            { keyName: "num_0", btnName: "numPadBtn0" },
            { keyName: "num_1", btnName: "numPadBtn1" },
            { keyName: "num_2", btnName: "numPadBtn2" },
            { keyName: "num_3", btnName: "numPadBtn3" },
            { keyName: "num_4", btnName: "numPadBtn4" },
            { keyName: "num_5", btnName: "numPadBtn5" },
            { keyName: "num_6", btnName: "numPadBtn6" },
            { keyName: "num_7", btnName: "numPadBtn7" },
            { keyName: "num_8", btnName: "numPadBtn8" },
            { keyName: "num_9", btnName: "numPadBtn9" },
            { keyName: "num_add", btnName: "numPadBtnPlus" },
            //{ keyName: "num_enter", btnName: "numPadBtnEnter" },
            { keyName: "enter", btnName: "numPadBtnEnter" },
            { keyName: "num_subtract", btnName: "numPadBtnMinus" },
            { keyName: "-", btnName: "numPadBtnMinus" },
            { keyName: "backspace", btnName: "numPadBtnBackspace" },
        ];

        for (let btn in buttonMapping)
        {
            krs.keypress.simple_combo(buttonMapping[btn].keyName, (function(btnId, keyName) {
                return function() {
                    //krs.log('Keypress keyName: ' + keyName, 'debug', false);
                    _clickButton(btnId);
                };
            })(buttonMapping[btn].btnName, buttonMapping[btn].keyName));
        }
    };

    let _init = function()
    {
        _clearDisplay();

        _registerOnClickEvents();
        _registerKeyboardEvents();

        _display.focus();

        _initDone();
    }();

    return {
        display: _display,
        apiCallBack: _apiCallBack
    }

})();