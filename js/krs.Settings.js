krs.namespace( 'krs.settings' );
krs.settings = (function()
{
    "use strict";
    let _settingsRefreshInterval = 60000;
    let _settings = {
        displayCore: {
            orderOverdueTimeout: "180000",
            updateOrderListInterval: "5000"
        }
    };

    let _processSettingsRow = function( setting, value )
    {
        let settingSplit = setting.split( '.' );
        if( !_settings.hasOwnProperty( settingSplit[ 0 ] ) )
        {
            _settings[ settingSplit[ 0 ] ] = [];
        }

        _settings[ settingSplit[ 0 ] ][ settingSplit[ 1 ] ] = value;
    };

    let _processSettings = function( apiResponse )
    {
        for( let setting in apiResponse )
        {
            if( !apiResponse.hasOwnProperty( setting ) )
            {
                continue;
            }

            _processSettingsRow( setting, apiResponse[ setting ] );
        }
    };

    let _fetchSettings = function()
    {
        krs.api.getSync( "settings/get", krs.settings.apiCallBackProcessSettings );
    };

    let _init = function()
    {
        _fetchSettings();

      //  setInterval( _fetchSettings, _settingsRefreshInterval );
    };

    $( document ).ready( _init );

    let _returnDisplayCore = function()
    {
        if(!_settings.hasOwnProperty("displayCore"))
        {
            _settings[ "displayCore" ] = [];
        }

        return _settings.displayCore;
    };

    return {
        apiCallBackProcessSettings: _processSettings,
        displayCore: _returnDisplayCore(),
        all: _settings,
    }
})();