krs.namespace( 'krs.DisplayCore' );
krs.DisplayCore = (function()
{
    "use strict";
    let _updateOrderListInterval = 5000;
    let _waitAnimationUpdateInterval = 30000;
    let _lastWaitAnimation = "logo.gif";
    let isWaitAnimationVisible = false;

    let _orderList = $( '#orderList' );

    let _waitAnimationContainer = $( "#waitContainer" );
    let _waitAnimation = $("#waitAnimation");

    /**
     * Add an order to the order list
     *
     * @param orderID {int}
     * @param readySince {int}
     */
    let _addToOrderList = function( orderID, readySince )
    {
        let newOrderListItem = document.createElement( "li" );
        let curda = new Date();
        newOrderListItem.innerHTML = "<b>" + orderID.toString() + "</b>";// + "#" + curda.getHours() + ":" + curda.getMinutes();

        if( readySince > parseInt(krs.settings.displayCore.orderOverdueTimeout) )
        {
            $( newOrderListItem ).addClass( "overdue" );
        }

        _orderList.append( newOrderListItem );
    };

    let _processOrder = function( order )
    {
        let actualDate = new Date().getTime();
        let cIn = new Date( order.cIn ).getTime();
        let readySince = (actualDate - cIn);

        _addToOrderList( order.cId, readySince );
    };

    let _processOrderList = function( apiResponse )
    {
        _orderList.empty();

        let ordersReady = false;

        for( let order in apiResponse )
        {
            if( !apiResponse.hasOwnProperty( order ) )
            {
                continue;
            }

            _processOrder( apiResponse[ order ] );
            ordersReady = true;
        }

        ordersReady ? hideWaitAnimation() : showWaitAnimation();
        console.log("ordersReady: " + ordersReady);
    };

    let showWaitAnimation = function()
    {
        _waitAnimationContainer.show();
        isWaitAnimationVisible = true;
    };

    let hideWaitAnimation = function()
    {
        _waitAnimationContainer.hide();
        isWaitAnimationVisible = false;
    };

    let _updateOrderList = function()
    {
        krs.api.get( "order/list", krs.DisplayCore.processOrderList );
    };

    let _initDone = function()
    {
        krs.log( '[krs.DisplayCore] init done', 'debug', false );
    };


    let _toggleOverdueAlarm = function()
    {
        _orderList.find(":not(.overdue)").removeClass("overdueAlarm");
        _orderList.find(".overdue").toggleClass("overdueAlarm");
    };

    let _updateWaitAnimation = function()
    {
        if(isWaitAnimationVisible)
        {
            krs.api.get( "waitAnimation/get", krs.DisplayCore.updateWaitAnimationCallback );
        }
    };

    let _updateWaitAnimationCallback = function(apiResponse)
    {
        if(apiResponse.hasOwnProperty("cFileName") &&
            _lastWaitAnimation === apiResponse.cFileName)
        {
            return;
        }

        _lastWaitAnimation = apiResponse.cFileName;

        let newAnimationPath = "../gifs/" + _lastWaitAnimation;

        _waitAnimation.hide();
        _waitAnimation.attr( "src", newAnimationPath );
        _waitAnimation.show(0);
    };

    let _init = function()
    {
        console.log(krs.settings.displayCore.updateOrderListInterval);
        setInterval( _updateOrderList, krs.settings.displayCore.updateOrderListInterval );
        setInterval( _toggleOverdueAlarm, 1000);
        //setInterval( _updateWaitAnimation, 1200);

        _waitAnimationContainer.hide();

        _initDone()
    };

    $( document ).ready( _init );

    return {
        "processOrderList": _processOrderList,
        "updateWaitAnimationCallback": _updateWaitAnimationCallback
    }

})();