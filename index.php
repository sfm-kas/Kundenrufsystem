<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KRS</title>

    <script src="/krs/js/bower_components/jquery/dist/jquery.min.js"></script>
</head>

<h1><a href="/krs/html/kitchen.html">Kitchen</a></h1>
<h1><a href="/krs/html/counter.html">Counter</a></h1>
<h1><a href="/krs/html/display.html">Display</a></h1>
<br>
<br>
<h1><a href="/krs/html/api/v1/waitAnimation/refreshFileList.php" target="_blank">refresh wait animation list</a></h1>


</body>
<script type="text/javascript">
    "use strict";
    let doIt = function()
    {
        console.log("doIt");
        setInterval(function(){
            window.location.pathname = "krs/html/display.html";
        }, 30000)
    };

    $(document).ready(doIt());
</script>
</html>
