let k;
k = (function($)
{
    "use strict";
    // private properties
    let _buttonConfirm = true,
        _buttonTimeOut = 3000, //in ms
        _warningTimeOut = 10000, //in ms
        _refreshInterval = 500, //in ms
        _updateInterval = 2000, // in ms
        _buttonCount = 3,

        _refreshActive = false,
        _updateActive = false;

    // private methods
    let _btnRemove = function()
        {
            for(let btn of $('.btnRemoveMarker'))
            {
                let now = new Date().getTime();
                if(((now - $(btn).data("markedOn")) > _buttonTimeOut) || _buttonTimeOut === null)
                {
                    $(btn).remove();
                }
            }
        },

        _btnMarkForRemoval = function(button)
        {
            let btn = $(button);
            btn.addClass("btnRemoveMarker");
            btn.data("markedOn", new Date().getTime());
            btn.removeClass("btn-danger");
        },

        _removeO = function(button)
        {
            let btn = $(button);
            let val = btn.val();
            //apiPost('remove_id.php', val);

            _btnMarkForRemoval(btn);

            if(_buttonTimeOut === null || !Number.isInteger(_buttonTimeOut))
            {
                _btnRemove();
            }
        },

        _btnRemoveRemovalMarker = function(button)
        {
            let btn = $(button);
            btn.removeClass("btnRemoveMarker");
            btn.removeClass("btn-warning");
            btn.data("markedOn", 0);
            btn.data("clickCount", 0);
        },

        _buttonOnClick = function()
        {
            let btn = $(this);

            if(btn.hasClass("btnRemoveMarker"))
            {
                _btnRemoveRemovalMarker(btn);
                return;
            }

            if(_buttonConfirm && (typeof btn.data("clickCount") === "undefined" || btn.data("clickCount") === 0))
            {
                btn.data("clickCount", 1);
                btn.removeClass("btn-danger");
                btn.addClass("btn-warning");
                return;
            }

            btn.addClass("btn-warning");

            _removeO(btn);
        },

        _startRemovalInterval = function()
        {
            if(!Number.isInteger(_buttonTimeOut))
            {
                return;
            }

            setInterval(_btnRemove, _buttonTimeOut);
        },

        _msToString = function(ms)
        {
            let seconds = (ms / 1000).toFixed(0);
            let minutes = Math.floor(seconds / 60);
            let hours = "";
            if(minutes > 59)
            {
                hours = Math.floor(minutes / 60);
                hours = (hours >= 10) ? hours : "0" + hours;
                minutes = minutes - (hours * 60);
                minutes = (minutes >= 10) ? minutes : "0" + minutes;
            }

            seconds = Math.floor(seconds % 60);
            seconds = (seconds >= 10) ? seconds : "0" + seconds;
            if(hours !== "")
            {
                return hours + ":" + minutes + ":" + seconds;
            }
            return minutes + ":" + seconds;
        },

        _clearByObj = function(button)
        {
            let btn = $(button);
            btn.html("");
            btn.val();
            btn.prop("disabled", true);
            btn.data("clickCount", 0);
            btn.data("markedOn", 0);
            btn.data("added", 0);
            btn.hide();
        },

        _clearByNr = function(nr)
        {
            let btn = $("#btn" + nr);
            _clearByObj(btn);
        },

        _refresh = function()
        {
            if(_refreshActive)
            {
                return;
            }

            while(_updateActive)
            {
                //waiting for the update to finish
            }

            _refreshActive = true;

            for(let button of $('.btn'))
            {
                let btn = $(button);

                if(btn.prop("disabled"))
                {
                    continue;
                }

                let warningTimer = dayjs().valueOf() - btn.data("added");
                let timer = _msToString(warningTimer);
                btn.find('.warningTimer').html(timer);
                btn.find('.oId').html(btn.val());

                if(warningTimer > _warningTimeOut && !btn.hasClass('btn-warning'))
                {
                    btn.addClass('btn-danger');
                }
            }

            _refreshActive = false;
        },

        _update = function(btnList)
        {
            while(_refreshActive)
            {
                //waiting for the update to finish
            }

            _updateActive = true;

            let i = 0;
            for(; i < btnList.length; i++)
            {
                let btn = $('#btn' + i);

                btn.val(btnList[i].id);
                btn.data("added", btnList[i].added);
                btn.prop('disabled', false);
                btn.show();
            }

            for(; i < _buttonCount; i++)
            {
                _clearByNr(i);
            }

            _updateActive = false;

            _refresh();
        },

        _fetchList = function()
        {
            //apiGet('latest');
            let json = [
                {
                    id: 1111,
                    added: 1533563843827,
                    markedOn: 0
                },
                {
                    id: 2222,
                    added: 1533560133000,
                    markedOn: 0
                }
            ];

            _update(json);
        },

        _startRefreshInterval = function()
        {
            setInterval(_refresh, _refreshInterval);
        },

        _startUpdateInterval = function()
        {
            //setInterval(_fetchList, _updateInterval);
            _fetchList(); //for testing we use a single call instead of an interval
        },

        _init = function()
        {
            $("#container1").on("click", '.btn', _buttonOnClick);

            _startRemovalInterval();

            _startUpdateInterval();

            _startRefreshInterval();

        };

    $(document).ready(_init);

    // revealing public API
    return {
        init: _init,
    };

}(jQuery));


let l;
l = (function($)
{
    "use strict";
    let _log = [],
        _logList = $("#logList"),
        _maxLogEntries = 200;

    let _toggleLogView = function()
        {
            $("#logWrapper").toggleClass("closed");
        },

        _init = function()
        {
            $("#logWrapper").on("click", _toggleLogView);
        },

        _cleanUp = function()
        {
            let logEntries = $('#log li');
            if(logEntries.count > _maxLogEntries)
            {
                logEntries.last().remove();
            }
        },

        _add = function(msg)
        {
            let timeStr = dayjs().format('[HH:mm:ss]');
            let newLogEntry = document.createElement("li");
            newLogEntry.innerHTML = timeStr + " " + msg;

            _logList.insertBefore(newLogEntry);

            _cleanUp();
        };

    $(document).ready(_init);

    // revealing public API
    return {
        add: _add,
        init: _init
    };

}(jQuery));